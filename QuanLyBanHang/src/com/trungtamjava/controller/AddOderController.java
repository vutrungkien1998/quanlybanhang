package com.trungtamjava.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.trungtamjava.model.Cart;
import com.trungtamjava.model.CartItem;
import com.trungtamjava.model.User;
import com.trungtamjava.service.CartItemService;
import com.trungtamjava.service.CartService;
import com.trungtamjava.service.impl.CartItemServiceImpl;
import com.trungtamjava.service.impl.CartServiceImpl;
@WebServlet(urlPatterns="/admin/oder/add")
public class AddOderController extends HttpServlet {
	
	CartService cartService= new CartServiceImpl();
	CartItemService cartItemService= new CartItemServiceImpl();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String buyerId= req.getParameter("buyerId");
		
		// tạo đơn
		Cart cart= new Cart();
		cart.setBuyDate(new Date());
		
		User buyer= new User();
		buyer.setId(Integer.parseInt(buyerId));
		
		cart.setBuyer(buyer);
		// thêm sản phẩm vào đơn hàng
		cartService.insert(cart);
		
		
		
		HttpSession session= req.getSession();
		
		Object object= session.getAttribute("cart");
		if(object!=null) {
			Map<Integer, CartItem> map = (Map<Integer, CartItem>) object;

			Set<Integer> keys = map.keySet();
			for (Integer key : keys) {
				CartItem cartItem = map.get(key);
				cartItem.setCart(cart);
				cartItemService.insert(cartItem);
			}
			
		}
		resp.sendRedirect(req.getContextPath()+"/thanhcong");
		
	}
}
