package com.trungtamjava.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.trungtamjava.model.CartItem;
import com.trungtamjava.model.Product;
import com.trungtamjava.service.ProductService;
import com.trungtamjava.service.impl.ProductServiceImpl;
@WebServlet(urlPatterns="/admin/cart-item/add")// ?productid=1
public class CartAddController extends HttpServlet {
	ProductService productService= new ProductServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String productId= req.getParameter("productId");
		Product product= productService.getByProductId(Integer.parseInt(productId));
		
		if(product!=null) {
			// check session
			// kiểm tra xem ssesion tồn tại hay chưa
			HttpSession session= req.getSession();
			
			Object object= session.getAttribute("cart");
			
			if(object==null) {
				// thêm mặt hàng vào giỏ hàng
				CartItem cartItem= new CartItem();
				cartItem.setBuyQuantity(1);
				cartItem.setProduct(product);
				cartItem.setSellPrice(product.getPrice());
				
				// cho vào danh sách mặt hàng mua
				Map<Integer,CartItem> map= new HashMap<>(); 
				map.put(product.getId(), cartItem);
				// cập nhật vào session cart
				session.setAttribute("cart", map);
			}else {
				Map<Integer,CartItem> map= (Map<Integer, CartItem>) object;
				
				// kiểm tra sản phảm đã có trong giỏ hàng chưa
				 CartItem item=map.get(product.getId());
				 if(item==null) {
					 CartItem cartItem= new CartItem();
						cartItem.setBuyQuantity(1);
						cartItem.setProduct(product);
						cartItem.setSellPrice(product.getPrice());
						
						map.put(product.getId(), cartItem);
				 }else {
					 item.setBuyQuantity(item.getBuyQuantity()+1);
				 }
				// cập nhật vào session cart
					session.setAttribute("cart", map);
			}
			
		}
		
		//chuyển về trang giỏ hàng
		resp.sendRedirect(req.getContextPath()+"/cart/list");
	}
}
