package com.trungtamjava.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
@WebServlet(urlPatterns ="/image")
public class DowloadImageController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName=req.getParameter("fileName");
		String location="F:\\anh_product\\";
		File file= new File(location+ fileName);
		resp.setContentType("image/jsp");
		IOUtils.copy(new FileInputStream(file), resp.getOutputStream());
	}
}
