package com.trungtamjava.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;
import com.trungtamjava.service.impl.UserServiceImpl;

@WebServlet(urlPatterns = "/admin/edit/user")
public class EditUserController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserService userService = new UserServiceImpl();
		String id = req.getParameter("uid");
		User user = userService.get(Integer.parseInt(id));

		req.setAttribute("user", user);
		RequestDispatcher dispatcher = req.getRequestDispatcher("/View/admin/user/editUser.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			UserService userService= new UserServiceImpl();
			User user= new User();
			DiskFileItemFactory factory= new DiskFileItemFactory();
			
			ServletFileUpload servletFileUpload= new ServletFileUpload(factory);
			List<FileItem> items= servletFileUpload.parseRequest(req);
			
			for(FileItem item: items) {
				if(item.getFieldName().equals("id")) {
					String id= item.getString();
					user.setId(Integer.parseInt(id));
				}
				else if(item.getFieldName().equals("name")) {
					String name=item.getString();
					user.setName(name);
				}else if(item.getFieldName().equals("username")) {
					String username=item.getString();
					user.setuName(username);
				}else if(item.getFieldName().equals("pass")) {
					String pass= item.getString();
					user.setPass(pass);
				}else if(item.getFieldName().equals("role")) {
					String role= item.getString();
					user.setRole(role);
				}else if(item.getFieldName().equals("filename")) {
					String location="F:\\anh\\";
					String fileName= System.currentTimeMillis()+".jsp";
					File file= new File(location + fileName);
					
					item.write(file);
					user.setAvatarFileName(fileName);
				}
				userService.update(user);
				resp.sendRedirect(req.getContextPath()+"/admin/list/user");
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
}
