package com.trungtamjava.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;
import com.trungtamjava.service.impl.UserServiceImpl;
@WebServlet(urlPatterns="/admin/list/user")
public class ListUserController extends HttpServlet {
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserService userService= new UserServiceImpl();
		List<User> list= userService.search("");
		
		req.setAttribute("userList", list);// list la obj
		
		
		RequestDispatcher dispatcher= req.getRequestDispatcher("/View/admin/user/listUser.jsp");
		dispatcher.forward(req, resp);
	}
}
