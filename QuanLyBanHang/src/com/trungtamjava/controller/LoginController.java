package com.trungtamjava.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;
import com.trungtamjava.service.impl.UserServiceImpl;

@WebServlet(urlPatterns = "/dang-nhap")
public class LoginController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String err = req.getParameter("err");

		if (err != null && err.equals("1")) {
			// loi sai ten dang nhap
			req.setAttribute("msg", "sai ten dang nhap hoac mat khau");

		}

		RequestDispatcher dispatcher = req.getRequestDispatcher("/View/Login.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserService userService = new UserServiceImpl();

		String uName = req.getParameter("username");
		String pass = req.getParameter("password");

		System.out.println(uName);
		System.out.println(pass);

		User user = userService.getByUsername(uName, pass);
		if (user != null) {
			HttpSession session = req.getSession();
			session.setAttribute("user", user);
			
				boolean t=user.getRole().equals("ADMIN");
				if(t== true) {
					resp.sendRedirect(req.getContextPath() + "/admin/add/user");
				}else {
					resp.sendRedirect(req.getContextPath() + "/member/list/product");
				}
				
			
			return;
		} else {
			resp.sendRedirect(req.getContextPath() + "/dang-nhap?err=1");
		}

	}
}
