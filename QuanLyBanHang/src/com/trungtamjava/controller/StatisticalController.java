package com.trungtamjava.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.trungtamjava.model.Cart;
import com.trungtamjava.service.CartService;
import com.trungtamjava.service.impl.CartServiceImpl;
@WebServlet(urlPatterns="/admin/thongke")
public class StatisticalController extends HttpServlet{// thống kê đơn hàng
	CartService cartService= new CartServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Cart> carts= cartService.getAll();
		req.setAttribute("cart", carts);
		
		RequestDispatcher dispatcher= req.getRequestDispatcher("/View/admin/cart/statiscal.jsp");
		dispatcher.forward(req, resp);
	}
}
