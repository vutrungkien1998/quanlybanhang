package com.trungtamjava.controller.clien;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;
import com.trungtamjava.service.impl.UserServiceImpl;

public class Myacount extends HttpServlet {
	UserService userSerive= new UserServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		Object object= req.getAttribute("user");
		User user= (User) object;
		userSerive.get(user.getId());
		req.setAttribute("user", user);
		RequestDispatcher dispatcher= req.getRequestDispatcher("");
		dispatcher.forward(req, resp);
		
	}
}
