package com.trungtamjava.controller.clien;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.trungtamjava.model.Category;
import com.trungtamjava.model.Product;
import com.trungtamjava.service.CategoryService;
import com.trungtamjava.service.ProductService;
import com.trungtamjava.service.impl.CategoryServiceImpl;
import com.trungtamjava.service.impl.ProductServiceImpl;
@WebServlet(urlPatterns="/member/category")
public class SearchCategory extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		CategoryService categoryService= new CategoryServiceImpl();
		List<Category> list1= categoryService.getAll();
		req.setAttribute("category", list1);
		
		ProductService productService= new ProductServiceImpl();
		String id= req.getParameter("categoryId");
		List<Product> list= productService.getByCategoryId(Integer.parseInt(id));
		req.setAttribute("product", list);
		RequestDispatcher dispatcher= req.getRequestDispatcher("/View/client/searchCategory.jsp");
		dispatcher.forward(req, resp);
		
	}
}
