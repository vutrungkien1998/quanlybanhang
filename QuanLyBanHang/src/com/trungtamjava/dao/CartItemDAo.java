package com.trungtamjava.dao;

import java.util.List;

import com.trungtamjava.model.CartItem;

public interface CartItemDAo {
	void input(CartItem cartItem);

	void update(CartItem cartItem);

	void delete(int id);

	CartItem get(int id);

	List<CartItem> list();
}
