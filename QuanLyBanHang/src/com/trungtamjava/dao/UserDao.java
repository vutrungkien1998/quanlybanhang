package com.trungtamjava.dao;

import java.util.List;

import com.trungtamjava.model.User;

public interface UserDao {
	void create(User u);

	void update(User u);
	
	void delete(int id);
	
	User get(int id);
	
	User getByUsername(String username);
	
	List<User> search(String name);
}
