package com.trungtamjava.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.trungtamjava.dao.CartItemDAo;
import com.trungtamjava.dao.JDBCConnection;
import com.trungtamjava.model.CartItem;
import com.trungtamjava.model.Product;

public class CartItemDaoImpl extends JDBCConnection implements CartItemDAo {

	@Override
	public void input(CartItem cartItem) {
		Connection conn= super.getJDBCConnection();
		
		try {
			String sql="insert into cart_item (buy_quantity, id_product, sell_price, cart_id) values(?,?,?,?)";
			PreparedStatement statement= conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, cartItem.getBuyQuantity());
			statement.setInt(2, cartItem.getProduct().getId());
			statement.setInt(3, cartItem.getSellPrice());
			statement.setInt(4, cartItem.getCart().getId());
			statement.executeUpdate();
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				cartItem.setId(id);// set vao doi tuong de su dung trong ham main sau nay
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(CartItem cartItem) {
		String sql = "UPDATE CartItem SET buy_quantity = ?, id_product = ?, sell_price = ? WHERE id = ?";
		Connection con = super.getJDBCConnection();

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, cartItem.getBuyQuantity());
			ps.setInt(2, cartItem.getProduct().getId());
			ps.setInt(3, cartItem.getSellPrice());
			ps.setInt(4, cartItem.getId());
						
			ps.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(int id) {
		Connection con = super.getJDBCConnection();

		try {
			String sql = "DELETE FROM cart_item WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public CartItem get(int id) {
		
		Connection con = super.getJDBCConnection();
		
		try {
			String sql ="SELECT cart_item.id, product.name, cart_item.buy_quantity, cart_item.sell_price " + 
					" FROM cart_item INNER JOIN product " + 
					" on cart_item.id_product= product.id ";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				CartItem cartItem= new CartItem();
				cartItem.setId(rs.getInt("id"));
				cartItem.setBuyQuantity(rs.getInt("buy_quantity"));
				cartItem.setSellPrice(rs.getInt("sell_price"));
				Product product= new Product();
				product.setName(rs.getString("name"));
				cartItem.setProduct(product);
				
				return cartItem;
								
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	@Override
	public List<CartItem> list() {
		List<CartItem> cartItemList = new ArrayList<CartItem>();
		Connection con = super.getJDBCConnection();
		
		try {
			String sql ="SELECT cart_item.id, product.name, cart_item.buy_quantity, cart_item.sell_price " + 
					" FROM cart_item INNER JOIN product " + 
					" on cart_item.id_product= product.id ";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				CartItem cartItem= new CartItem();
				cartItem.setId(rs.getInt("id"));
				cartItem.setBuyQuantity(rs.getInt("buy_quantity"));
				cartItem.setSellPrice(rs.getInt("sell_price"));
				Product product= new Product();
				product.setName(rs.getString("name"));
				cartItem.setProduct(product);
				
				cartItemList.add(cartItem);
								
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return cartItemList;
	}
	

}
