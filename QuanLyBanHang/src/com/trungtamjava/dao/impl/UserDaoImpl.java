package com.trungtamjava.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.trungtamjava.dao.JDBCConnection;
import com.trungtamjava.dao.UserDao;
import com.trungtamjava.model.User;

public class UserDaoImpl extends JDBCConnection implements UserDao {

	@Override
	public void create(User u) {
		Connection conn = super.getJDBCConnection();
		try {
			String sql = "insert into user(name, username, password, role, file_name) values(?,?,?,?,?)";
			PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, u.getName());
			statement.setString(2, u.getuName());
			statement.setString(3, u.getPass());
			statement.setString(4, u.getRole());
			statement.setString(5, u.getAvatarFileName());
			statement.executeUpdate();

			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				u.setId(id);// set vao doi tuong de su dung trong ham main sau nay
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(User u) {
		Connection conn = super.getJDBCConnection();
		
		try {
			String sql = "update user set name=?, username=?, password=?, role=?, file_name=?, where ( id=?)";
			PreparedStatement statement= conn.prepareStatement(sql);
			statement.setString(1, u.getName());
			statement.setString(2, u.getuName());
			statement.setString(3, u.getPass());
			statement.setString(4, u.getRole());
			statement.setString(5, u.getAvatarFileName());
			statement.setInt(6, u.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(int id) {
		Connection conn = super.getJDBCConnection();
		try {
			String sql = "DELETE FROM user WHERE id = ? ";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setInt(1, id);

			statement.executeUpdate();
		} catch (Exception e) {
			System.out.println("Loi CSDL: " + e);
		} finally {
			// dong ket noi toi database
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public User get(int id) {
		Connection conn= super.getJDBCConnection();
		try {
			String sql = "SELECT * FROM user WHERE id =?";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setInt(1, id);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				User p = new User();
				p.setId(id);
				p.setName(rs.getString("name"));
				p.setuName(rs.getString("username"));
				p.setPass(rs.getString("password"));
				p.setRole(rs.getString("role"));
				p.setAvatarFileName(rs.getString("file_name"));
				return p;
			}
		} catch (Exception e) {
			System.out.println("Loi CSDL: " + e);
		} finally {
			// dong ket noi toi database
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public User getByUsername(String username) {
		Connection conn= super.getJDBCConnection();
		try {
			String sql = "SELECT * FROM user WHERE username =?";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setString(1, username);

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				User p = new User();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
				p.setuName(rs.getString("username"));
				p.setPass(rs.getString("password"));
				p.setRole(rs.getString("role"));
				p.setAvatarFileName(rs.getString("file_name"));
				return p;
			}
		} catch (Exception e) {
			System.out.println("Loi CSDL: " + e);
		} finally {
			// dong ket noi toi database
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	

	@Override
	public List<User> search(String name) {
		Connection conn= super.getJDBCConnection();
		List<User> list= new ArrayList<User>();
		try {
			String sql = "SELECT * FROM user WHERE name like ?";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setString(1, "%" + name + "%");

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				User p = new User();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
				p.setuName(rs.getString("username"));
				p.setPass(rs.getString("password"));
				p.setRole(rs.getString("role"));
				p.setAvatarFileName(rs.getString("file_name"));
				list.add(p);
			}
		} catch (Exception e) {
			System.out.println("Loi CSDL: " + e);
		} finally {
			// dong ket noi toi database
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
}
