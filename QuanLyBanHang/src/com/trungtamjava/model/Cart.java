package com.trungtamjava.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Cart implements Serializable {// đối tượng giỏ hàng, ser
	private int id;
	private User buyer;// người mua hàng
	private Date buyDate;// ngày mua
	private List<CartItem> cartItems;
	private String note; // 1 đơn hàng có n cartitem
	
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getBuyer() {
		return buyer;
	}
	public void setBuyer(User buyer) {
		this.buyer = buyer;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public List<CartItem> getCartItems() {
		return cartItems;
	}
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}
	
	

}
