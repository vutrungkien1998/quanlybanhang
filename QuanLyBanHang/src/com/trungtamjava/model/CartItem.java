package com.trungtamjava.model;

import java.io.Serializable;

public class CartItem implements Serializable{
	private int id;
	private Product product;
	private int buyQuantity;// số lượng sản phẩm mua
	private int sellPrice;// giá bán tại thời diểm mua
	private Cart cart; // đơn hàng
	
	
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getBuyQuantity() {
		return buyQuantity;
	}
	public void setBuyQuantity(int buyQuantity) {
		this.buyQuantity = buyQuantity;
	}
	public int getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(int sellPrice) {
		this.sellPrice = sellPrice;
	}
	
	
		
}
