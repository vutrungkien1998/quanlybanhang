package com.trungtamjava.service;

import java.util.List;

import com.trungtamjava.model.Category;

public interface CategoryService {
	public void add(Category c);

	void edit(Category p);

	void delete(int id);

	List<Category> getAll();
}
