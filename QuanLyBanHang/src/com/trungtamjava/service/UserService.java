package com.trungtamjava.service;

import java.util.List;

import com.trungtamjava.model.User;

public interface UserService {
	void iput(User user);

	void update(User user);

	void delete(int id);

	User get(int id);

	User getByUsername(String username, String pass);

	List<User> search(String name);
}
