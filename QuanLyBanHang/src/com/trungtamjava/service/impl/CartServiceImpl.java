package com.trungtamjava.service.impl;

import java.util.List;

import com.trungtamjava.dao.CartDao;
import com.trungtamjava.dao.impl.CartDaoImpl;
import com.trungtamjava.model.Cart;
import com.trungtamjava.model.CartItem;
import com.trungtamjava.service.CartService;

public class CartServiceImpl implements CartService {
	CartDao cartDao= new CartDaoImpl();
	@Override
	public void insert(Cart cart) {
		cartDao.insert(cart);
		
	}

	@Override
	public void edit(Cart newCart) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		cartDao.delete(id);
		
	}

	@Override
	public Cart get(int id) {
		// TODO Auto-generated method stub
		return cartDao.get(id);
	}

	@Override
	public List<Cart> getAll() {
		// TODO Auto-generated method stub
		return cartDao.getAll();
	}

	@Override
	public List<CartItem> search(int id) {
		// TODO Auto-generated method stub
		return cartDao.search(id);
	}

}
