package com.trungtamjava.service.impl;

import java.util.List;

import com.trungtamjava.dao.ProducDao;
import com.trungtamjava.dao.impl.ProductDaoImpl;
import com.trungtamjava.model.Product;
import com.trungtamjava.service.ProductService;

public class ProductServiceImpl implements ProductService {

	@Override
	public void add(Product product) {
	ProducDao producDao= new ProductDaoImpl();
	producDao.add(product);
		
	}

	@Override
	public void edit(Product newproduct) {
		ProducDao producDao= new ProductDaoImpl();
		Product oldProduct= producDao.getByProductId(newproduct.getId());
		if(oldProduct!= null) {
			oldProduct.setName(newproduct.getName());
			
			oldProduct.setPrice(newproduct.getPrice());
			oldProduct.setQuantity(newproduct.getQuantity());
			oldProduct.setDescribe(newproduct.getDescribe());
			if(newproduct.getImage()!=null) {
				oldProduct.setImage(newproduct.getImage());
			}
			producDao.edit(oldProduct);
		}
		
		
	}

	@Override
	public void delete(int id) {
		ProducDao producDao= new ProductDaoImpl();
		producDao.delete(id);
		
	}

	@Override
	public List<Product> getAll(String name) {
		ProducDao productDao= new ProductDaoImpl();
		return productDao.getAll(name);
	}

	@Override
	public Product getByProductId(int id) {
		ProducDao productDao= new ProductDaoImpl();
		return productDao.getByProductId(id);
	}

	@Override
	public List<Product> getByCategoryId(int id) {
		ProducDao productDao= new ProductDaoImpl();
		return productDao.getByCategoryId(id);
	}

	@Override
	public List<Product> searchByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
