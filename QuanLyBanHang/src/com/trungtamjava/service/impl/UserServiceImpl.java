package com.trungtamjava.service.impl;

import java.util.List;

import com.trungtamjava.dao.UserDao;
import com.trungtamjava.dao.impl.UserDaoImpl;
import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public void iput(User user) {
		UserDao userDao= new UserDaoImpl();
		userDao.create(user);
		
	}

	@Override
	public void update(User newuser) {
		UserDao userDao= new UserDaoImpl();
		User oldUser= userDao.get(newuser.getId());
		if(oldUser!=null) {
			//convert data tu newUser sang oldUSer
			oldUser.setName(newuser.getName());
			// .. cac thuoc tinh khac
			oldUser.setuName(newuser.getuName());
			oldUser.setPass(newuser.getPass());
			oldUser.setRole(newuser.getRole());
			// neu upload anh moi thi update lai
			
			if(newuser.getAvatarFileName() !=null) {
				oldUser.setAvatarFileName(newuser.getAvatarFileName());
			}
		}
		userDao.update(oldUser);
		
	}

	@Override
	public void delete(int id) {
		UserDao userDao= new UserDaoImpl();
		userDao.delete(id);
		
	}

	@Override
	public User get(int id) {
		UserDao userDao= new UserDaoImpl();
		return userDao.get(id);
	}

	@Override
	public User getByUsername(String username, String pass) {
		UserDao userDao= new UserDaoImpl();
		User user =userDao.getByUsername(username);
		if(user!= null) {
			if(user.getPass().equals(pass)) {
				return user;
			}
		}

		return null;
	}

	@Override
	public List<User> search(String name) {
		UserDao userDao= new UserDaoImpl();
		return userDao.search(name);
	}

}
